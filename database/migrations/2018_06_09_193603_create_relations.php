<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('shelters', function (Blueprint $table) {

            $table->foreign('worker_id')->references('id')->on('workers')->onDelete('cascade');
            $table->foreign('cat_id')->references('id')->on('cats')->onDelete('cascade');
        });
        Schema::table('cats', function (Blueprint $table) {

            $table->foreign('worker_id','guardian')->references('id')->on('workers')->onDelete('cascade');
            $table->foreign('shelter_uskey')->references('uskey')->on('shelters')->onDelete('cascade');
        });

        Schema::table('workers', function (Blueprint $table) {

            $table->foreign('cat_id')->references('id')->on('cats')->onDelete('cascade');
            $table->foreign('shelter_uskey')->references('uskey')->on('shelters')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
