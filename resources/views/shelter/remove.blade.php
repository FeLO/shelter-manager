@extends('layouts.app',['shelters',$shelters])


@section('content')
    <h1>Remove Shelter</h1>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">City</th>
            <th scope="col">Size</th>
            <th scope="col">Remove</th>
        </tr>
        </thead>
        <tbody>
        @if(count($shelters)>0)

        @foreach($shelters as $shelter)
        <tr>
            <th scope="row">{{$loop->index+1}}</th>
            <td>{{$shelter->name}}</td>
            <td>{{$shelter->city}}</td>
            <td>{{$shelter->size}}</td>
            <td>
                {!!Form::open(['action' => ['SheltersController@destroy' ,$shelter->uskey] , 'method' => 'DELETE' , 'class' => 'pull-right'])!!}
                {{Form::submit('Delete', ['class'=>'btn btn-danger'])}}
                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
        @endif

        </tbody>
    </table>


@endsection
