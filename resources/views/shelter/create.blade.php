@extends('layouts.app',['shelters',$shelters])


@section('content')
    <h1>Add Shelter</h1>

    {!! Form::open(['action' => 'SheltersController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
    <div class="form-group">
        <div class="col-3">
        {{Form::label('name', 'Name')}}
        {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Name'])}}
        </div>
    </div>
    <div class="form-group">
        <div class="col-3">
            {{Form::label('city', 'City')}}
            {{Form::text('city', '', ['class' => 'form-control', 'placeholder' => 'City'])}}
        </div>
    </div>
    <div class="form-group">
        <div class="col-3">
            {{Form::label('size', 'Size')}}
            {{Form::text('size', '', ['class' => 'form-control', 'placeholder' => '0'])}}
        </div>
    </div>
    <div class="form-group">
        <div class="col-3">
        {{Form::submit('Submit', ['class'=>'btn btn-primary'])}}
        </div>
    </div>
    {!! Form::close() !!}

@endsection
