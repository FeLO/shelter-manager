<nav class="navbar navbar-expand navbar-dark bg-dark">
    <a class="navbar-brand" href="/about">Shelter-manager v1.0</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExample02">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
            </li>
            @if(@count($shelters)>0)
                @foreach( $shelters as $shelter)
                <li class="nav-item">
                        <a class="nav-link" href="/shelter/{{$shelter->uskey}}">{{$shelter->name}}</a>
                    </li>
                @endforeach
            @endif


        </ul>

        <p><a class="btn btn-success btn-lg" href="/shelter/create"  role="button" >Add</a> <a class="btn btn-danger btn-lg" href="/shelter/remove" role="button">Remove</a></p>



    </body>
    </html>
        //
    </div>
</nav>