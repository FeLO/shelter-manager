



<div class="btn-group-vertical">
<div class="btn-group mr-2" role="group" aria-label="Second group">
    <a class="btn btn-outline-secondary" href="/shelter/{{$shelter->uskey}}/cat"  role="button" >Cats</a>
    <a class="btn btn-outline-secondary" href="/shelter/{{$shelter->uskey}}/worker" role="button">Workers</a>
</div>

<br/>
    {!! Form::open(['action' => [''.$controller.'@store',$shelter->uskey,$id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}

    <div class="btn-group mr-2 form-inline">
<div class="btn-group-vertical">
    {{Form::submit('Add', ['class'=>'btn btn-primary mb-2'])}}
</div>
    <div class="form-group mx-sm-3 mb-2">
        <div class="col-3">
            {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Name'])}}
        </div>
    </div>

    <div class="form-group mx-sm-3 mb-2">
        <div class="col-3">
            @if(strpos($controller,'Cat')!==false)
            {{Form::text('color', '', ['class' => 'form-control', 'placeholder' => 'Color'])}}
            @elseif(strpos($controller,'Worker')!==false)
                {{Form::text('age', '', ['class' => 'form-control', 'placeholder' => 'Age'])}}
            @endif
        </div>
    </div>

</div>
    {!! Form::close() !!}
</div>