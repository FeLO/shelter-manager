@if($table || count($table)>0)
    <ul class="list-group">
        @foreach($table as $element)
            <li class="list-group-item">
                <div class="float-left">
                {{$element->name}}
                </div>
                <div class="float-right">
                {!!Form::open(['action' => [''.$controller.'@destroy' ,$element->id,$id] , 'method' => 'DELETE' , 'class' => 'pull-right'])!!}
                {{Form::submit('Delete', ['class'=>'btn btn-sm btn-danger'])}}
                {!! Form::close() !!}
                </div>
            </li>
        @endforeach
    </ul>
@endif