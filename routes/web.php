<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//MAIN PAGES
Route::get('/', 'PagesController@index');
Route::get('/about', 'PagesController@about');

//SHELTER
Route::get('/shelter/remove', 'SheltersController@remove');
Route::resource('shelter','SheltersController');

Route::resource('/shelter/{shelter}/cat','CatsController');
Route::resource('/shelter/{shelter}/worker','WorkersController');





