<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cat extends Model
{
    //

    protected $fillable=['name','color'];
    function guardian()
    {
        return $this->belongsTo('App\Worker','worker_id');
    }
    function shelter()
    {
        return $this->belongsTo('App\Shelter');
    }

}
