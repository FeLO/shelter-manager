<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Worker extends Model
{
    //

    protected $fillable=['name','age'];
    function cats()
    {
       return $this->hasMany('App\Cat');
    }
    function shelters()
    {
        return $this->hasMany('App\Shelter');
    }

}
