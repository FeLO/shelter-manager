<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shelter extends Model
{
    //
    protected $shelters='sh';

    protected $primaryKey = 'uskey';

    protected $fillable=['uskey','name','city','size'];
    public $incrementing = false;
    function workers()
    {
        return $this->hasMany('App\Worker');
    }

    function cats()
    {
        return $this->hasMany('App\Cat');
    }

    public static function generateUSkey()
    {

        $lowerChars="abcdefghijklmnopqrstuvwxyz";
        $upperChars="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $numbers="0123456789";

        $front=substr(str_shuffle($numbers),0,1);
        $middle=substr(str_shuffle($lowerChars),0,3);
        $end=substr(str_shuffle($upperChars),0,1);

        return $front.$middle.$end;
    }
}
