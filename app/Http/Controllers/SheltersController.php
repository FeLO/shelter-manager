<?php

namespace App\Http\Controllers;

use App\Shelter;
use Illuminate\Http\Request;

class SheltersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return redirect('/');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $shelters=Shelter::all()->sortBy('created_at');
        return view('shelter.create')->with('shelters',$shelters);
    }

    public function remove()
    {

        $shelters=Shelter::all()->sortBy('created_at');
        return view('shelter.remove')->with('shelters',$shelters);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'name'=>'required',
            'city'=>'required',
            'size'=>'required',
        ]);

        //create NEW shelter
        $shelter= Shelter::create([
            "uskey" => Shelter::generateUSKey(),
            "name" => $request->input('name'),
            "city" => $request->input('city'),
            "size" => $request->input('size')
        ]);

        $shelter->save();


        return redirect('/shelter/create')->with('success','New shelter created');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        //$shelters=Shelter::ALL();
        //$shelter= Shelter::find($id);
        //return view('shelter.cat.index')->with(compact('shelter','shelters','id'));
        return redirect('/shelter/'.$id.'/cat')->with('id',$id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $shelter = Shelter::find($id);

        $shelter->delete();


        return redirect('/shelter/remove')->with('success','Shelter deleted');
    }
}
