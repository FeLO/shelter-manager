<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shelter;
use App\Cat;
class CatsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        //

        $shelters=Shelter::ALL()->sortBy('created_at');
        $shelter= Shelter::find($id);
        $cats = $shelter->cats()->get();

        //wyszukiwanie kotow !
        return view('shelter.cat.index')->with(compact('shelter','shelters','cats'));



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id,Request $request)
    {
        //

        //
        $this->validate($request,[
            'name'=>'required',
            'color'=>'required'
        ]);

        //create NEW cat
        $cat = Cat::create([
            'name'=>$request->input('name'),
            'color'=>$request->input('color')
        ]);

        $shelter = Shelter::find($id);
        $shelter->cats()->save($cat);


        return redirect('/shelter/'.$id.'/cat')->with('success','New cat added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        //wyszukiwanie kotow !
        return view('shelter.cat.index')->with(compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $cat = Cat::find($id);

        $cat->delete();


        return redirect()->back()->with('success','Cat deleted');
    }
}
