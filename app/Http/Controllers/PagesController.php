<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shelter;
class PagesController extends Controller
{
    public function index()
    {
        $shelters=Shelter::all()->sortBy('created_at');
        return view('pages.index')->with('shelters',$shelters);
    }
    public function about()
    {
        $shelters=Shelter::all()->sortBy('created_at');
        return view('pages.about')->with('shelters',$shelters);
    }
}
