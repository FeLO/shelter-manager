<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shelter;
use App\Worker;
class WorkersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        //

        $shelters=Shelter::ALL()->sortBy('created_at');
        $shelter= Shelter::find($id);
        $workers=$shelter->workers()->get();

        return view('shelter.worker.index')->with(compact('shelter','shelters','workers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id,Request $request)
    {
        //
        //

        //
        $this->validate($request,[
            'name'=>'required',
            'age'=>'required'
        ]);

        //create NEW worker
        $worker = Worker::create([
            'name'=>$request->input('name'),
            'age'=>$request->input('age')
        ]);

        $shelter = Shelter::find($id);
        $shelter->workers()->save($worker);

        return redirect('/shelter/'.$id.'/worker')->with('success','New worker added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view('shelter.worker.index')->with(compact('shelter','shelters','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $worker = Worker::find($id);

        $worker->delete();


        return redirect()->back()->with('success','Worker deleted');
    }
}
